# -*- coding: utf8 -*-
## fa.py 1.0
## Copyright Djiembou Victor (15/06/2021)
## 
## git@gitlab.com:Viclegrand28/info304.git
## https://gitlab.com/Viclegrand28/info304.git
## 
## Viclegranddab@gmail.com

#import pydotplus
import os
import sys
import subprocess
import copy
from collections import defaultdict
from package.disjoint_set import DisjointSet
from types import FunctionType
from package.functions import *
#from IPython.display import Image

__version__ = '1.0'

def Property(func) :
    return property(**func())

def methods(cls):
    return [x for x, y in cls.__dict__.items() if type(y) == FunctionType]

class FA:
    """ This class represent any type of finite automaton."""

    def __init__(self, alphabet="", states=[], transitions={}, finals=[], init=[]):
        """[summary]
        @param param alphabet is string of symbol of language
        @param states is set of state of fa
        @param transition is fa delta function which determine from src_state where we go
        @param finals is set of state on which word construct on the language can end
        @param init is set of state on which word must start his lecture.
        """

        """ Initialise the finite automate.
            @param name the name of the automate."""
        self.name = ""
        """ List of string corresponding to states name.
            States are always identificated by name."""
        self.states = states
        """ Dictionary using src state as key and mapping it to a list of
            pair (dest_state, symbol)."""
        self.transitions = transitions
        """ The string corresponding to the name of the initial state."""
        self.init = init
        """ A list containing the name of the final states."""
        self.finals = finals
        """ A string containing all symbols in the alphabet."""
        self.alphabet = ""
        for s in alphabet:
            if s not in self.alphabet:
                self.alphabet += s

    def setName(self, name):
        self.name = name
    
    def getName(self):
        return self.name

    def add_state(self, state, init = False, final = False):
        """[summary]
        add automaton state
        """

        if final:
            self.finals.append(state)
        if init:
            self.init.append(state)
        if state in self.states:
            print("error : state '" + state + "' already exists.")
            return
        self.transitions[state] = []
        self.states.append(state)

    def valid_symbol(self, symbol):
        """[summary]
        determine if a symbol is proper to alphabet symbol
        """

        if symbol not in self.alphabet: return False
        return True

    def dst_state(self, src_state, symbol):
        """[summary]
        determine which state we attend using a @param symbol on @param src_state
        """

        if src_state not in self.states:
            print("error : the state '" + src_state + "' is not an existing state.")
            return
        add = []
        state_transitions = self.transitions[src_state]     
        for (s,d) in state_transitions:
            if s == symbol:
                add.append(d)
        return add

    def add_transition(self, src_state, symbol, dst_state):
        """[summary]
        @param src_state is a state from which we need to go anywhere using a symbol
        @param symbol is an alphabet symbol on which we may use to attend a dst_state
        @param dst_state is a state on which we can attend using a symbol from a src_state.
        """

        if not self.valid_symbol(symbol):
            print("error : the symbol '" + symbol + "' is not part of the alphabet.")
            return
        if src_state not in self.states:
            print("error : the state '" + src_state + "' is not an existing state.")
            return
        if dst_state not in self.states:
            print("error : the state '" + dst_state + "' is not an existing state.")
            return
        if (symbol, dst_state) in self.transitions[src_state]:
            print("error : the transition (" + src_state + ", " + symbol + ", ...) already exists.")
            return
        self.transitions[src_state].append((symbol, dst_state))

    def __str__(self):
        """[summary]
        __str__ return the presentation of finite automaton on string format
        """

        ret = "FA :\n"
        ret += "-name:'" + self.name + "'\n"
        ret += "-alphabet:'" + self.alphabet + "'\n"
        ret += "-init:" + str(self.init) + "\n"
        ret += "-finals:" + str(self.finals) + "\n"
        ret += "- states (%d) :\n" % (len(self.states))
        for state in self.states:
            ret += "- (%s)" % (state)
            if len(self.transitions[state]) == 0:
                ret += ".\n"
            else:
                ret += ":\n"
                for (sym, dest) in self.transitions[state]:
                    ret += "--(%s)--> (%s)\n" % (sym, dest)
            
        return ret

    def checkA(self, dst_state):
        """[summary]
        iterative function that determine if @param dst_state is attending from @param src_state
        """

        blacklist = []
        begin = self.init
        while len(begin) != 0:
            temp = []
            for state in begin:
                if not state in blacklist: blacklist.append(state)
                current_state = self.transitions[state]
                for (s,d) in current_state:
                    if not d in blacklist: 
                        blacklist.append(d)
                        temp.append(d)
                    if d == dst_state: return True
            begin = temp
        return False

    def checkC(self, state):
        """[summary]
        iterative function that determine if we can attend final state from @param state
        """
        blacklist = []
        begin = [state]
        while len(begin) != 0:
            temp = []
            for state in begin:
                if not state in blacklist: blacklist.append(state)
                current_state = self.transitions[state]
                for (s,d) in current_state:
                    if not d in blacklist: 
                        blacklist.append(d)
                        temp.append(d)
                    if d in self.finals: return True
            begin = temp
        return False

    def is_accessible_state(self, dst_state):
        """[summary]
        determine if @param state is attending from initial state
        """

        flag = False
        if dst_state in self.states:
            if dst_state in self.init: return True
            else:
                    flag = flag or self.checkA(dst_state)
        else: return "{} it's not an automaton state".format(dst_state)
            
        return flag

    def is_co_accessible_state(self, state):
        """[summary]
        determine if from @param state we can attend final state
        """

        flag = False
        if state in self.states:
            if state in self.finals:
                return True
            else:
                return self.checkC(state)
        else:
            return "it's not an automaton state"

    def is_usefull_state(self, state):
        """[summary]
        determine if @param state is accessible and co-accessible
        """

        return (self.is_accessible_state(state) and self.is_co_accessible_state(state))

    def is_emonde(self):
        """[summary]
        determine if all automaton state are usefull
        """

        flag = True
        for state in self.states:
            flag = flag and self.is_usefull_state(state)
        return flag

    def recognize(self, word, verbose=False):
        """[summary]
        check if @param word passed is in our language
        """

        flag = False
        ends = []
        for state in self.init:
            current_state = [state]
            i = 0
            for symbol in word:
                if verbose:
                    print("configuration : (" +
                          current_state + ", " + word[i:] + ")")
                if not self.valid_symbol(symbol):
                    print("error : the symbol '" + symbol +
                          "' is not part of the alphabet. Abord.")

                add = []
                for substate in current_state:
                    add  = add + self.dst_state(substate,symbol)

                    print("transition available for (" +
                                substate + ", " + symbol + ","+ str(add)+").")
                    

                if len(add) != 0 : current_state = add
                i = i+1
            for current in current_state:
                if current in self.finals:
                    if verbose:
                        print("ending on final state '" + current + "'.")
                    ends.append(current)
                    flag = True
        if verbose:
            print("ending on state(s) '" + str(ends) + "'")
        return flag

    def to_dot(self):
        """ Returns a string corresponding to the specified self in DOT format.
        @param self  the self to be converted in DOT format.
        @param name the name of the automaton for the DOT file ("Graph")
            by default.
        @returns the automaton in DOT format."""

        ret = "digraph " + ((self.name.replace("(", "_")).replace(")", "_")).replace(",", "_") + " {\n    rankdir=\"LR\";\n\n"
        ret += "    // States (" + str(len(self.states)) + ")\n"

        def state_name(s): return "Q_" + str(self.states.index(s))

        # States
        # Initial state
        ret += "    node [shape = point ];     __Qi__ // Initial state\n"
        for state in self.states:
            ret += "    "
            if state in self.finals:
                ret += "node [shape=doublecircle]; "
            else:
                ret += "node [shape=circle];       "
            ret += state_name(state) + " [label=" + ((state.replace("(", "")).replace(")", "")).replace(",", "") + "];\n"

        # Transitions
        ret += "\n    // Transitions\n"
        ret += "    __Qi__ -> " + \
            state_name(self.init[0]) + "; // Initial state arrow\n"
        for state in self.states:
            for (symbol, dst_state) in self.transitions[state]:
                ret += "    " + \
                    state_name(state) + " -> " + state_name(dst_state) + \
                    " [label=" + symbol + "];\n"
        ret += "}\n"
        tmp_file ="{}.dot".format(self.name)
        with open(tmp_file, "w") as file:
            file.write(ret)
        return ret

    def display(self):
        """[summary]
        review automaton
        """
        self.to_dot()

    def save(self, filename='graphout.png'):
        """[summary]
        save automation as graph image
        """

        try:
            pipe = subprocess.Popen(
                ['dot', '-Tpng', '-o', filename], stdin=subprocess.PIPE)
            pipe.stdin.write(self.to_dot().encode('ascii') + b'\n')
            pipe.stdin.close()
            pipe.wait()
        except OSError as e:
            print(e)

    def mirror(self):
        """[summary]
        return the mirror of automaton
        mirror automaton is an automaton in which 
            - init = self.finals
            - finals = self.init
            - all transitions are inversed
        """
        ret = ''
        if isinstance(self, NFA):
            ret = NFA(self.alphabet,self.states.copy(), self.transitions.copy(), self.finals.copy(), self.init.copy())
        elif isinstance(self, ENFA):
            ret = ENFA(self.alphabet,self.states.copy(), self.transitions.copy(), self.finals.copy(), self.init.copy())
        else:
            ret = DFA(self.alphabet,self.states.copy(), self.transitions.copy(), self.finals.copy(), self.init.copy())
        ret.init.clear()
        ret.finals.clear()
        ret.states.clear()
        ret.transitions.clear()
        for s in self.states:
            ret.add_state(s)
        ret.init=self.finals.copy()
        ret.finals=self.init.copy()
        for s in ret.states:
            for symbol in self.alphabet:
                temp_states=self.dst_state(s,symbol)
                for t in temp_states:
                    ret.add_transition(t,symbol,s)
        ret.name = "Mirror({})".format(self.name)
        return ret

    def typeof(self):
        """[summary]
        return the type of automaton
        """

        if "E" in str(self.alphabet) :
            return 0 # automate à transitions spontanées
        else :
            nb_init=len(self.init) #nombres d'états initials
            if nb_init>1:
                return 2 #Automate fini non deterministe
            else :
                for state in self.states: 
                    for symbol in self.alphabet:
                        if len(self.dst_state(state,symbol))>=2:
                            return 2
                return 1 #Automate fini deterministe

    def is_complete(self):
        """[summary]
        determine if an automaton is complete or that all state can transite on each symbol of alphabet
        """

        for state in self.states:
            for symbol in self.alphabet:
                if len(self.dst_state(state, symbol))== 0:
                    return False
        return True

    def complete(self):
        """[summary]
        complete an automation with add @param Qp state which will get all undefine transitions
        """
        autom = ''
        if isinstance(self, NFA):
            autom = NFA(self.alphabet,self.states.copy(), self.transitions.copy(), self.finals.copy(), self.init.copy())
        elif isinstance(self, ENFA):
            autom = ENFA(self.alphabet,self.states.copy(), self.transitions.copy(), self.finals.copy(), self.init.copy())
        else:
            autom = DFA(self.alphabet,self.states.copy(), self.transitions.copy(), self.finals.copy(), self.init.copy())
        if not autom.is_complete():
            qp = "Qp"+autom.states[len(autom.states)-1]
            autom.add_state(qp)
            for state in autom.states:
                for symbol in autom.alphabet:
                    if len(autom.dst_state(state, symbol)) == 0:
                        autom.add_transition(state, symbol, qp)
            autom.name = "complete({})".format(self.name)
        return autom

    def star(self):
        """[summary]
        start FA
        """
        if self.typeof()!=0:
            alphabet=self.alphabet+"E"
        else :
            alphabet=self.alphabet
        start = ''
        if isinstance(self, NFA):
            start = NFA(alphabet,self.states.copy(), self.transitions.copy(), self.finals.copy(), self.init.copy())
        elif isinstance(self, ENFA):
            start = ENFA(alphabet,self.states.copy(), self.transitions.copy(), self.finals.copy(), self.init.copy())
        else:
            start = DFA(alphabet,self.states.copy(), self.transitions.copy(), self.finals.copy(), self.init.copy())
        start.init.clear()
        start.finals.clear()
        start.states.clear()
        start.transitions.clear()

        for s in self.states:
            final = s in self.finals
            start.add_state(s,init=False,final=final)

        for i in self.init:
            start.init.append(i)
        start.transitions=copy.deepcopy(self.transitions)
        for f in self.finals:
            for i in self.init:
                start.add_transition(f,"E",i)
        start.name = "start({})".format(self.name)
        return start

    def intersect(self, autom):
        """[summary]
        intersect of two FAs
        """
        intersect = self.product(autom)
        for elt in intersect.states:
            temp = elt.split(',')
            flag = False
            cpt = 0
            for subs in temp:
                if (subs in self.finals) or (subs in autom.finals):
                    flag = True
                    cpt+=1
            if flag and (cpt == len(temp)):
                intersect.add_state(elt, init=False, final=True)
        intersect.name = "intersect({},{})".format(self.name,autom.name)
        return intersect
    
    def difference(self, autom):
        """[summary]
        difference of two FAs
        """
        difference = self.product(autom)
        for elt in difference.states:
            temp = elt.split(',')
            flag = False
            for subs in temp:
                if (subs in self.finals) and not(subs in autom.finals):
                    flag = True
                elif subs in autom.finals: flag = False
            if flag:
                difference.add_state(elt, init=False, final=True)
        difference.name = "difference({},{})".format(self.name,autom.name)
        return difference
    
    def union(self, autom):
        """[summary]
        union of two FAs
        """
        union = self.product(autom)
        for elt in union.states:
            temp = elt.split(',')
            flag = False
            for subs in temp:
                if (subs in self.finals) or (subs in autom.finals):
                    flag = True
            if flag:
                union.add_state(elt, init=False, final=True)
        union.name = "union({},{})".format(self.name,autom.name)
        return union

    def product(self, fa):
        """[summary]
        concat or product of two FAs
        """
        # create the new alphabet of product automaton
        alpha = ""
        for elt in self.alphabet + fa.alphabet:
            if alpha == "": alpha = elt
            elif not(elt in alpha): alpha+=elt
        # use the copy method to create a new memory reference of each properties for the new FA
        autom = ''
        if isinstance(self, NFA):
            autom = NFA(alpha,self.states.copy(), self.transitions.copy(), self.finals.copy(), self.init.copy())
        elif isinstance(self, ENFA):
            autom = ENFA(alpha,self.states.copy(), self.transitions.copy(), self.finals.copy(), self.init.copy())
        else:
            autom = DFA(alpha,self.states.copy(), self.transitions.copy(), self.finals.copy(), self.init.copy())
        # use the clear object method to erase the content of these properties
        autom.init.clear()
        autom.finals.clear()
        autom.states.clear()
        autom.transitions.clear()
        # construct the compound init states of product automaton
        init = ""
        for elt in self.init+fa.init:
            if init == "": init = elt
            else:init+=','+elt
        # add the initial state of out new FA
        autom.add_state(init, init=True, final=False)
        # fetch the automata to find were we'll reach using our alphabet symbol
        for elt in autom.states:
            for symbol in autom.alphabet:
                new = ""
                temp = elt.split(',')
                for subs in temp:
                    dst = []
                    if self.valid_symbol(symbol) and (subs in self.states):
                        dst = self.dst_state(subs, symbol)
                        #print("self {}+{}-->{}".format(symbol,subs,dst))
                    elif fa.valid_symbol(symbol) and (subs in fa.states):
                        dst = fa.dst_state(subs,symbol)
                        #print("fa {}+{}-->{}".format(symbol,subs,dst))
                    for c in dst:
                        if new == "": new=c
                        elif not(c in new): new+=','+c
                if new != "":
                    autom.add_state(new, init=False, final=False)
                    autom.add_transition(elt, symbol, new)
        #print("""self:{}""".format(self))
        return autom

class NFA(FA):
    """ This class represent non deterministic finite automaton."""

    def __init__(self, alphabet="", states=[], transitions={}, finals=[], init=[]):
        FA.__init__(self,alphabet, states, transitions, finals, init)
    
    """[summary]
    convert our nfa to a deterministic finite automaton
        with:
            - one initial state
            - each state has |alphabet| transition
            - some finals states
            - state here can be list.

    """
    def determinism(self) :
        dfa = DFA(self.alphabet,self.states.copy(), self.transitions.copy(), self.finals.copy(), self.init.copy())
        dfa.init.clear()
        dfa.finals.clear()
        dfa.states.clear()
        dfa.transitions.clear()
        dfa.add_state(join(self.init,",")) #create a unique init state and add to states variable
        dfa.init=[] #create an empty list of init state conform to our class structure 
        dfa.init.append(join(self.init,",")) # add the unique init state inside
        r=0 # there is the number of state pass by determinism method
        while r<len(dfa.states):
            for symbol in dfa.alphabet :
                temp_states=dfa.states[r].split(",") # split our current in his substates
                j=0
                new_state=""
                while j<len(temp_states):
                    if len(self.dst_state(temp_states[j],symbol))>0: #check all destination state base on current symbol
                        k=0
                        while k<len(self.dst_state(temp_states[j],symbol)): #fetch all destination states
                            if new_state=="":
                                new_state=self.dst_state(temp_states[j],symbol)[k] #add those states in the new state
                            else : 
                                if self.dst_state(temp_states[j],symbol)[k] not in new_state.split(","): # check if the destination state is not yet exist in new state variable
                                    
                                    new_state=new_state+","+self.dst_state(temp_states[j],symbol)[k] #add those states in the new state
                            k=k+1
                    j=j+1
                if new_state!="":
                    temp=new_state.split(",") #split the new state as list
                    temp.sort() #sort the list
                    new_state=join(temp,",") #reconvert the list of states to string
                    final=False #flag to know if content a final state
                    if new_state not in dfa.states: # check if new state is newer
                        for f in self.finals : # loop for checking if new state content final state
                            if f in new_state.split(","): # check if inide
                                final=True
                                break #break if final state showed
                        dfa.add_state(new_state,init=False,final=final) #add our new state
                        #print(new_state)
                        #print(final)
                    dfa.add_transition(dfa.states[r],symbol,new_state) #add transition to our new state
                    # print("transition de %s vers %s sur %s "%(dfa.states[r],new_state,symbol))
            r=r+1
        dfa.name = "determinism({})".format(self.name)
        return dfa 

class DFA(FA):
    """ This class represent any type of deterministic finite automaton."""

    def __init__(self, alphabet="", states=[], transitions={}, finals=[], init=[]):
        """ Initialise the finite automaton.
            @param the alphabet of the automaton."""
        FA.__init__(self,alphabet, states, transitions, finals, init)

    def negate(self):
        """[summary]
        negate of FA
        """
        if not self.is_complete():
            print("error : negation requires a complete DFA.")
            return
        
        negate = DFA(self.alphabet,self.states.copy(), self.transitions.copy(), self.finals.copy(), self.init.copy())
        oldfinals = negate.finals.copy()
        negate.finals.clear()
        #print(""".DS_Store {}, {}, {}""".format(negate.finals, negate.states,oldfinals))
        for state in negate.states:
            if state not in oldfinals:
                negate.finals.append(state)
        negate.name = "negate({})".format(self.name)
        return negate

    def _remove_unreachable_states(self):
        g = defaultdict(list)
        for k,v in self.transitions.items():
            for (s,x) in v:
                #print(x)
                g[k].append(x)
        stack = [self.init]
        #print(stack)
        reachable_states =  set()
        while stack:
            state = stack.pop()
            #print(state)
            if state[0] not in reachable_states:
                for d in g[state[0]]:
                    stack.append(d)
            reachable_states.add(state[0]) if state[0] not in reachable_states else None
        print(g)
        print(stack)
        self.states = [state for state in self.states if state in reachable_states]

        self.final_states = [state for state in self.finals if state in reachable_states]


        self.transitions = { k:v for k,v in self.transitions.items() if k in reachable_states}

    def minimize(self):
        minim = DFA(self.alphabet,self.states.copy(), self.transitions.copy(), self.finals.copy(), self.init.copy())
        minim._remove_unreachable_states()

        def order_tuple(a,b):
            return (a,b) if a < b else (b,a)

        table = {}

        sorted_states = minim.states

        # initialize the table
        for i,item in enumerate(sorted_states):
            for item_2 in sorted_states[i+1:]:
                table[(item,item_2)] = (item in minim.finals) != (item_2 in minim.finals)

        flag = True
        print(table)
        # table filling method
        while flag:
            flag = False

            for i,item in enumerate(sorted_states):
                for item_2 in sorted_states[i+1:]:

                    if table[(item,item_2)]:
                        continue

                    # check if the states are distinguishable
                    for w in minim.alphabet:
                        t1 = None
                        t2 = None
                        for (symbol, dst_state) in minim.transitions[item]:
                            if symbol == w:
                                t1 = dst_state
                                print("dst1-{}-{}".format(w,t1))
                        for (symbol, dst_state) in minim.transitions[item_2]:
                            if symbol == w:
                                t2 = dst_state
                                print("dst2-{}-{}".format(w,t2))

                        if not(t1 == None) and not(t2 == None) and (t1 != t2):
                            if not str(t1).isdigit():
                                marked = table[(t1,t2)]

                            marked = table[order_tuple(t1,t2)]
                            flag = flag or marked
                            table[(item,item_2)] = marked
                            
                            #if marked:
                                #break

        d = DisjointSet(minim.states)

        # form new states
        for k,v in table.items():
            print("{} {}".format(k,v))
            if not v:
                d.union(k[0],k[1])
        print(d.get())
        t = []
        for x in d.get():
            st = ""
            for e in x:
                if st == "": st = e
                else: st+=","+e
            t.append(st)

        minim.states = t
        print(minim.states)
        new_final_states = []
        minim.init = [str(d.find_set(minim.init[0]))]

        for s in minim.states:
            for item in s:
                if item in minim.finals:
                    new_final_states.append(s)
                    break
        a = {}
        for k,v in minim.transitions.items():
            print(" {} -- {}".format(k,v))
            if type(v) == type([]) and len(v) != 0:
                for (s,y) in v:
                    print(y)
                    if type(a.get(str(d.find_set(k)))) == type([]) and not((s,str(d.find_set(y))) in a.get(str(d.find_set(k)))):
                        a[str(d.find_set(k))].append((s,str(d.find_set(y))))
                    else:
                        a.update({str(d.find_set(k)):[(s,str(d.find_set(y)))]})
            elif len(v) == 0:
                a.update({str(d.find_set(k)):[]})
            else:
                a.update({str(d.find_set(k)):[(s,str(d.find_set(v[1])))]})
        minim.transitions = a
        minim.finals = new_final_states
        minim.name = "minimize({})".format(self.name)
        print("""
        init : {}
        states : {}
        alpha : {}
        final : {}
        transi : {}
        """.format(minim.init,minim.states,minim.alphabet,minim.finals,minim.transitions))
        return minim

    def canonize(self):
        m = self.minimize()
        return m.complete()
class ENFA(FA):
    """ This class represent any type of non deterministic finite automaton with E transition."""

    def __init__(self, alphabet="", states=[], transitions={}, finals=[], init=[]):
        """ Initialise the non deterministic finite automaton with E transition.
            @param the alphabet of the automaton."""
        FA.__init__(self,alphabet, states, transitions, finals, init)
    
    def closure(self,state):
        """[summary]
        determine e-closure of @param state
        return list of state that we can reach only using E transition
        """
        eclosure = [state]
        for substate in eclosure:
            eclosure  += self.dst_state(substate,"E")
        add = []
        for elt in eclosure:
            if len(add)==0:add.append(elt)
            else:
                if not (elt in add): add.append(elt)
        eclosure = add
        return eclosure

    def convertToNFA(self):
        """[summary]
        Convert E-NFA to NFA by removing all E transitions
        return NFA
        """
        #print(self)
        alphabet = ""
        for elt in self.alphabet: # remove E on alphabet
            if elt != "E":
                alphabet+=elt
        #nfa = NFA(alphabet) # instance an NFA base on E-NFA alphabet restrict E symbol
        nfa = NFA(alphabet,self.states.copy(), self.transitions.copy(), self.finals.copy(), self.init.copy())
        nfa.init.clear()
        nfa.finals.clear()
        nfa.states.clear()
        nfa.transitions.clear()
        for state in self.states: # fetch all states of E-NFA to remove E Transition
            temp = self.closure(state) # get e-closure of current state

            if state in self.init:
                nfa.add_state(state, init = True, final = False)

            flagf = False
            for current in temp: # fetch list of e-closure to add state in new NFA 
                if current in self.finals:
                    flagf = True # current state is a final state
                nfa.add_state(current, init = False, final = False) # current state be a state

            if flagf:
                nfa.add_state(state, init = False, final = True) # add to final nfa states

            for symbol in nfa.alphabet: # fetch nfa symbols
                trans = [] # list of state reachable on @symbol from @temp list of states
                for elt in temp:
                    trans += self.dst_state(elt, symbol) # get transition base on @symbol transition
                
                temp1 = [] # list of state reachable on E from @trans list of states
                for elt in trans:
                    temp1 += self.closure(elt)

                add = [] # list of not multiple states on @temp1
                for elt in temp1:
                    if len(add)==0:add.append(elt)
                    else:
                        if not (elt in add): add.append(elt)
                temp1 = add

                for elt in temp1: # for each state in @temp1 add transition (@state,@symbol,@elt)
                    nfa.add_transition(state, symbol, elt)
        nfa.name = "converttonfa_{}_".format(self.name)
        return nfa # return nfa

    def determinism(self):
        """[summary]
        Convert E-NFA to DFA by removing all E transitions
        return DFA
        """
        nfa = self.convertToNFA()
        return nfa.determinism()

