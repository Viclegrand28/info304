# -*- coding: utf-8 -*-
"""
Created on Sat Jun 12 09:57:08 2021

@author: brice
"""
import DFA
import NDFA
import copy
def run(dfa, word, verbose = False):
    if dfa.init == None:
        print("error : the automaton does not have any initial symbol.")
        return False

    current_state = dfa.init

    i = 0
    for symbol in word:
        if verbose : print("configuration : (" + current_state + ", " + word[i:] + ")")
        if not dfa.valid_symbol(symbol):
            print("error : the symbol '" + symbol + "' is not part of the alphabet. Abord.")

        next_state = dfa.dst_state(current_state, symbol)
        if next_state is None:
            if verbose: print("no transition available for (" + current_state + ", " + symbol + ").")
            return False;

        current_state = next_state
        i = i+1

    if current_state in dfa.finals:
        if verbose: print("ending on final state '" + current_state + "'.")
        return True
    if verbose: print("ending on non accepting state '" + current_state + "'")
    return False

def test(ndfa, word, verbose = False):
    if len(ndfa.init) == 0:
        print("error : the automaton does not have any initial symbol.")
        return False

    current_state = ndfa.init

    i = 0
    for symbol in word:
        k=0
        next_state=[]
        while k < len(current_state):
            next_state = next_state + ndfa.dst_state(current_state[k], symbol)
            if verbose : 
                print("configuration : (" + current_state[k] + ", " + word[i:] + ")")
                print(ndfa.dst_state(current_state[k], symbol))
            if not ndfa.valid_symbol(symbol):
                print("error : the symbol '" + symbol + "' is not part of the alphabet. Abord.")
                return False
            k = k+1
        if(len(next_state))==0 :
            print("no transition avaible for (" + symbol + ")")
            return False
        current_state = next_state
        i = i+1
    k = 0
    result = False
    while k < len(current_state) :
        if current_state[k] in ndfa.finals:
           if verbose: 
               print("ending on final state '" + current_state[k] + "'.")
           result = result or True
        else : 
           if verbose: 
               print("ending on non accepting state '" + current_state[k] + "'")
           result = result or False
        k = k+1
        return result

def successors(dfa, state):
    if state not in dfa.states:
        print("error : the specified state '" + state + "' is not part of the automaton.")
        return

    ret = []
    for (symbol, dst_state) in dfa.transitions[state]:
        if dst_state not in ret:
            ret.append(dst_state)

    return ret

def predecessors(dfa, state):
    if state not in dfa.states:
        print("error : the specified state '" + state + "' is not part of the automaton.")
        return

    ret = []
    for src_state in dfa.states:
        for (symbol, dst_state) in dfa.transitions[src_state]:
            if dst_state == state and src_state not in ret:
                ret.append(src_state)

    return ret

def is_complete(dfa):
    for state in dfa.states:
        for symbol in dfa.alphabet:
            if dfa.dst_state(state, symbol) == None:
                return False
    return True

def complete(dfa):
    qp = "Qp"
    i = 0
    while qp in dfa.states:
        qp = "Qp" + str(i)
        i += 1

    dfa.add_state(qp)
    for state in dfa.states:
        for symbol in dfa.alphabet:
            if dfa.dst_state(state, symbol) == None:
                dfa.add_transition(state, symbol, qp)

    return dfa

def accessible_states(dfa):
    visited = []
    to_visit = [dfa.init]

    while len(to_visit) > 0:
        state = to_visit.pop()
        visited.append(state)
        for succ in successors(dfa, state):
            if succ not in visited and succ not in to_visit:
                to_visit.append(succ)

    return visited

def accessible(dfa, state):
    if state not in dfa.states:
        print("error : the state '" + state + "' is not part of the automaton.")
        return False

    return state in accessible_states(dfa)

def accessible(dfa):
    return len(dfa.states) == len(accessible_states(dfa))

def coaccessible_states(dfa):
    visited = []
    to_visit = dfa.finals.copy()

    while len(to_visit) > 0:
        state = to_visit.pop()
        visited.append(state)
        for pred in predecessors(dfa, state):
            if pred not in visited and pred not in to_visit:
                to_visit.append(pred)

    return visited

def coaccessible(dfa, state):
    if state not in dfa.states:
        print("error : the state '" + state + "' is not part of the automaton.")
        return

    return state in coaccessible_states(dfa)

def coaccessible(dfa):
    return len(dfa.states) == len(coaccessible_states(dfa))

def trim(dfa):
    return accessible(dfa) and coaccessible(dfa)

def negate(dfa):
    if not is_complete(dfa):
        print("error : negation requires a complete DFA.")
        return

    oldfinals = dfa.finals.copy()
    dfa.finals.clear()

    for state in dfa.states:
        if state not in oldfinals:
            dfa.finals.append(state)

    return dfa

def intersect(dfa1, dfa2):
    alphabet = set.union(set(list(dfa1.alphabet)), set(list(dfa2.alphabet)))
    ret = DFA.DFA(alphabet)
    to_visit = []
    def get_superstate(state1, state2):
        sstate = "{" + state1 + "," + state2 + "}"

        if sstate not in ret.states:
            is_final = state1 in dfa1.finals and state2 in dfa2.finals
            ret.add_state(sstate, is_final)
            to_visit.append((sstate, state1, state2))

        return sstate

    ret.init = get_superstate(dfa1.init, dfa2.init)

    while len(to_visit) > 0:
        (sstate, state1, state2) = to_visit.pop()

        for symbol in ret.alphabet:
            dst_state1 = dfa1.dst_state(state1, symbol)
            dst_state2 = dfa2.dst_state(state2, symbol)

            if dst_state1 is None or dst_state2 is None:
                continue

            dst_sstate = get_superstate(dst_state1, dst_state2)

            ret.add_transition(sstate, symbol, dst_sstate)

    return ret

def union(dfa1, dfa2):
    alphabet = set.union(set(list(dfa1.alphabet)), set(list(dfa2.alphabet)))
    ret = DFA.DFA(alphabet)
    to_visit = []
    def get_superstate(state1, state2):
        sstate = "{" + state1 + "," + state2 + "}"

        if sstate not in ret.states:
            is_final = state1 in dfa1.finals or state2 in dfa2.finals
            ret.add_state(sstate, is_final)
            to_visit.append((sstate, state1, state2))

        return sstate

    ret.init = get_superstate(dfa1.init, dfa2.init)

    while len(to_visit) > 0:
        (sstate, state1, state2) = to_visit.pop()

        for symbol in ret.alphabet:
            dst_state1 = dfa1.dst_state(state1, symbol)
            dst_state2 = dfa2.dst_state(state2, symbol)

            if dst_state1 is None or dst_state2 is None:
                continue

            dst_sstate = get_superstate(dst_state1, dst_state2)

            ret.add_transition(sstate, symbol, dst_sstate)

    return ret

def miror(dfa) :
    oldinit = dfa.init.copy()
    dfa.init.clear()
    dfa.init = dfa.finals.copy()
    dfa.finals.clear()
    dfa.final = oldinit
    alphabet = set(list(dfa.alphabet))
    dfa=DFA.DFA(alphabet)
    return dfa
    
def NDFA_to_DFA(ndfa) :
    alphabet = set(list(ndfa.alphabet))
    dfa=DFA.DFA(alphabet)
    dfa.add_state(join(ndfa.init,","))
    dfa.init=ndfa.init
    r=0
    while r<len(dfa.states):
        for symbol in dfa.alphabet :
            temp_states=dfa.states[r].split(",")
            j=0
            new_state=""
            while j<len(temp_states):
                if len(ndfa.dst_state(temp_states[j],symbol))>0:
                    k=0
                    while k<len(ndfa.dst_state(temp_states[j],symbol)):
                        if new_state=="":
                            new_state=ndfa.dst_state(temp_states[j],symbol)[k]
                        else :
                            if ndfa.dst_state(temp_states[j],symbol)[k] not in new_state.split(","):
                                new_state=new_state+","+ndfa.dst_state(temp_states[j],symbol)[k]
                        k=k+1
                j=j+1
            if new_state!="":
                temp=new_state.split(",")
                temp.sort()
                new_state=join(temp,",")
                final=False
                if new_state not in dfa.states:
                    for f in ndfa.finals :
                        if f in new_state.split(","):
                            final=True
                            break
                    print("new state {%s}"%(new_state))
                    dfa.add_state(new_state,final)
                dfa.add_transition(dfa.states[r],symbol,new_state)
            print("ajout de transition de {%s} vers {%s} sur {%s}"%(dfa.states[r],new_state,symbol))
        r=r+1
    return dfa

def join(a_list,sym):
    result=""
    k=0
    while k<len(a_list):
        if result=="":
            result=a_list[k]
        else :
            result=result+sym+a_list[k]
        k=k+1
    return result