# -*- coding: utf8 -*-
## re.py 1.0
## Copyright Djiembou Victor (15/06/2021)
##
## git@gitlab.com:Viclegrand28/info304.git
## https://gitlab.com/Viclegrand28/info304.git
##
## Viclegranddab@gmail.com

from package import stack, Automaton

__version__ = '1.0'


def Property(func):
    return property(**func())

class RE:
    def __init__(self,reg):
        self.reg = reg

    def union(self,ndfa1,ndfa2):
        alpha=set.union(set(list(ndfa1.alphabet)), set(list(ndfa2.alphabet)),set(list("E")))
        Ret=Automaton.Automaton(alpha)
        Ret.add_state("0")
        Ret.init=["0"]
        i=0
        for s in ndfa1.states:
            i=i+1
            Ret.add_state(str(i))
        for s in ndfa2.states:
            i=i+1
            Ret.add_state(str(i))
        #ajout des E-transitions
        Ret.add_transition("0","E",str(Ret.states[1]))
        Ret.add_transition("0","E",str(Ret.states[1+len(ndfa1.states)]))
        k=0
        for j in range(1,len(ndfa1.states)+1):
            state=ndfa1.states[k]
        for t in ndfa1.transitions[state]:
            Ret.add_transition(str(j),t[0],str(ndfa1.states.index(t[1])+1))
        k=k+1 
        k=0
        for j in range(len(ndfa1.states)+1,len(ndfa1.states)+len(ndfa2.states)+1):
            state=ndfa2.states[k]
            for t in ndfa2.transitions[state]:
                Ret.add_transition(str(j),t[0],str(len(ndfa1.states)+ndfa2.states.index(t[1])+1))
            k=k+1
        f=str(len(ndfa1.states)+len(ndfa2.states)+1)
        Ret.add_state(f,True)
        Ret.add_transition(str(len(ndfa1.states)),"E",f)
        Ret.add_transition(str(len(ndfa1.states)+len(ndfa2.states)),"E",f)
        return Ret

    def concat(self,ndfa1,ndfa2):
        if ndfa1!=None and ndfa2!=None:
            alpha=set.union(set(list(ndfa1.alphabet)), set(list(ndfa2.alphabet)))
            alpha=set.union(set(list(alpha)),set(list("E")))
            Ret=Automaton.Automaton(alpha)
            i=0
            for s in ndfa1.states:
                Ret.add_state(str(i))
                i=i+1
            for s in ndfa2.states:
                final=s in ndfa2.finals
                Ret.add_state(str(i),final)
                i=i+1
            Ret.init=["0"]
            #ajout de transitions
            Ret.add_transition(str(len(ndfa1.states)-1),"E",str(len(ndfa1.states)))
            k=0
            for j in range(0,len(ndfa1.states)):
                state=ndfa1.states[k]
            for t in ndfa1.transitions[state]:
                Ret.add_transition(str(j),t[0],str(ndfa1.states.index(t[1])))
            k=k+1 
            k=0
            for j in range(len(ndfa1.states),len(ndfa1.states)+len(ndfa2.states)):
                state=ndfa2.states[k]
                for t in ndfa2.transitions[state]:
                    Ret.add_transition(str(j),t[0],str(len(ndfa1.states)+ndfa2.states.index(t[1])))
                k=k+1
            return Ret
        elif ndfa1!=None :
            return ndfa1
        elif ndfa2!=None :
            return ndfa2
    def iterate(self,ndfa1):
        alpha=ndfa1.alphabet+"E"
        Ret=Automaton.Automaton(alpha)
        Ret.add_state("0")
        Ret.init=["0"]
        i=0
        for s in ndfa1.states:
            i=i+1
            Ret.add_state(str(i))
        #ajout des E-transitions
        k=0
        for j in range(1,len(ndfa1.states)+1):
            state=ndfa1.states[k]
        for t in ndfa1.transitions[state]:
            Ret.add_transition(str(j),t[0],str(ndfa1.states.index(t[1])+1))
        k=k+1 
        f=str(len(ndfa1.states)+1)
        Ret.add_state(f,True)
        Ret.add_transition("0","E",str(Ret.states[1]))
        Ret.add_transition("0","E",str(Ret.states[len(Ret.states)-1]))
        Ret.add_transition(str(len(ndfa1.states)),"E",str(Ret.states[len(Ret.states)-1]))
        Ret.add_transition(str(Ret.states[len(Ret.states)-2]),"E",str(Ret.states[1]))
        return Ret
    def simple(self,a):    
        Ret=Automaton.Automaton(a)
        Ret.add_state("0")
        Ret.add_state("1",True)
        Ret.init=["0"]
        Ret.add_transition("0",a,"1")
        return Ret

    def regex_to_nfa_thompson(self):
        alphabet=list("abcdefghijklmnopqrstuvwxyz")
        operators=stack.Stack()
        operands=stack.Stack()
        concat_stack=stack.Stack()
        para_count=0
        ccflag=False
        i=0
        for symbol in self.reg:
            if symbol in alphabet:
                print("1")
                operands.push(self.simple(symbol))
                if ccflag:
                    print("1a")
                    operators.push('.')
                else:
                    print("1b")
                    ccflag=True
            else :
                if symbol==')':
                    ccflag=False
                    if(para_count==0):
                        print("error in regular expression in position %d"%(i))
                        return False
                    else :
                        para_count=para_count-1
                    while(operators.empty()==False and operators.peek()!="("):
                        op=operators.pop()
                        if(op=="."):
                            nfa2=operands.pop()
                            nfa1=operands.pop()
                            operands.push(self.concat(nfa1,nfa2))
                        elif op=="+":
                            nfa2=operands.pop()
                            if(operators.empty()==False and operators.peek()=="."):
                                concat_stack.push(operands.pop())
                                while(operators.empty()==False and operators.peek()=="."):
                                    concat_stack.push(operands.pop())
                                    operators.pop()
                                nfa1=self.concat(concat_stack.pop(),concat_stack.pop())
                                while(concat_stack.empty==False):
                                    nfa1=self.concat(nfa1,concat_stack.pop())
                            else:
                                nfa1=operands.pop()
                            operands.push(self.union(nfa1,nfa2))
                    if operators.peek()=="(":
                        operators.pop()
                elif symbol=="*":
                        operands.push(self.iterate(operands.pop()))
                        ccflag=True
                elif symbol=="(":
                        operators.push('.')
                        ccflag=False
                        operators.push(symbol)
                        para_count=para_count+1
                elif symbol=="+":
                        operators.push(symbol)
                        ccflag=False
                i=i+1
        while operators.empty()==False :
            if operands.empty()==True:
                print("regular expression not correct")
                return False
            op=operators.pop()
            if op==".":
                nfa2=operands.pop()
                nfa1=operands.pop()
                if nfa1!=None and nfa2!=None:
                        operands.push(self.concat(nfa1,nfa2))
                elif nfa1!=None:
                        operands.push(nfa1)
                elif nfa2!=None:
                        operands.push(nfa2)
            elif op=="+":
                nfa2=operands.pop()
                if (operators.empty()==False and operators.peek()=="."):
                    concat_stack.push(operands.pop())
                    while(concat_stack.empty()==False and operators.peek()=="."):
                        concat_stack.push(operands.pop())
                        operators.pop()
                    nfa1=self.concat(concat_stack.pop(),concat_stack.pop())
                    while(concat_stack.empty()==False):
                        nfa1=self.concat(nfa1,concat_stack.pop())
                else :
                    nfa1=operands.pop()
                operands.push(self.union(nfa1,nfa2))
        return operands.pop()
