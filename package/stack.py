class Stack:
    def __init__(self):
        self.list=[]
    def push(self,Item):
        self.list.append(Item)
    def peek(self):
        if self.empty()==False :
            return self.list[len(self.list)-1]
    def empty(self):
        if len(self.list)==0:
            return True
        else :
           return False
    def pop(self):
        if self.empty()==False:
           p=self.list[len(self.list)-1]
           del self.list[len(self.list)-1]
           return p
    def display(self):
        print(self.list)