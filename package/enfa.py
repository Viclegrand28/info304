# -*- coding: utf8 -*-
# e-nfa.py 1.0
# Copyright Djiembou Victor (15/06/2021)
##
# git@gitlab.com:Viclegrand28/info304.git
# https://gitlab.com/Viclegrand28/info304.git
##
# Viclegranddab@gmail.com

from package.fa import FA
from package.nfa import NFA

__version__ = '1.0'


def Property(func):
    return property(**func())


class ENFA(FA):
    """ This class represent any type of non deterministic finite automaton with E transition."""

    def __init__(self, alphabet="", states=[], transitions={}, finals=[], init=[]):
        """ Initialise the non deterministic finite automaton with E transition.
            @param the alphabet of the automaton."""
        FA.__init__(self,alphabet, states, transitions, finals, init)
    
    def closure(self,state):
        """[summary]
        determine e-closure of @param state
        return list of state that we can reach only using E transition
        """
        eclosure = [state]
        for substate in eclosure:
            eclosure  += self.dst_state(substate,"E")
        add = []
        for elt in eclosure:
            if len(add)==0:add.append(elt)
            else:
                if not (elt in add): add.append(elt)
        eclosure = add
        return eclosure

    def convertToNFA(self):
        """[summary]
        Convert E-NFA to NFA by removing all E transitions
        return NFA
        """
        #print(self)
        alphabet = ""
        for elt in self.alphabet: # remove E on alphabet
            if elt != "E":
                alphabet+=elt
        #nfa = NFA(alphabet) # instance an NFA base on E-NFA alphabet restrict E symbol
        nfa = DFA(alphabet,self.states.copy(), self.transitions.copy(), self.finals.copy(), self.init.copy())
        nfa.init.clear()
        nfa.finals.clear()
        nfa.states.clear()
        nfa.transitions.clear()
        for state in self.states: # fetch all states of E-NFA to remove E Transition
            temp = self.closure(state) # get e-closure of current state

            if state in self.init:
                nfa.add_state(state, init = True, final = False)

            flagf = False
            for current in temp: # fetch list of e-closure to add state in new NFA 
                if current in self.finals:
                    flagf = True # current state is a final state
                nfa.add_state(current, init = False, final = False) # current state be a state

            if flagf:
                nfa.add_state(state, init = False, final = True) # add to final nfa states

            for symbol in nfa.alphabet: # fetch nfa symbols
                trans = [] # list of state reachable on @symbol from @temp list of states
                for elt in temp:
                    trans += self.dst_state(elt, symbol) # get transition base on @symbol transition
                
                temp1 = [] # list of state reachable on E from @trans list of states
                for elt in trans:
                    temp1 += self.closure(elt)

                add = [] # list of not multiple states on @temp1
                for elt in temp1:
                    if len(add)==0:add.append(elt)
                    else:
                        if not (elt in add): add.append(elt)
                temp1 = add

                for elt in temp1: # for each state in @temp1 add transition (@state,@symbol,@elt)
                    nfa.add_transition(state, symbol, elt)

        return nfa # return nfa

    def determinism(self):
        """[summary]
        Convert E-NFA to DFA by removing all E transitions
        return DFA
        """
        nfa = self.convertToNFA()
        return nfa.determinism()


