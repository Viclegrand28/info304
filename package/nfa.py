# -*- coding: utf8 -*-
## nfa.py 1.0
## Copyright Djiembou Victor (15/06/2021)
##
## git@gitlab.com:Viclegrand28/info304.git
## https://gitlab.com/Viclegrand28/info304.git
##
## Viclegranddab@gmail.com

from package.fa import FA
from package.dfa import DFA
from package.functions import *

__version__ = '1.0'


def Property(func):
    return property(**func())


class NFA(FA):
    """ This class represent non deterministic finite automaton."""

    def __init__(self, alphabet="", states=[], transitions={}, finals=[], init=[]):
        print (FA)
        FA.__init__(self,alphabet, states, transitions, finals, init)
    
    """[summary]
    convert our nfa to a deterministic finite automaton
        with:
            - one initial state
            - each state has |alphabet| transition
            - some finals states
            - state here can be list.

    """
    def determinism(self) :
        dfa = DFA(self.alphabet.copy(),self.states.copy(), self.transitions.copy(), self.finals.copy(), self.init.copy())
        dfa.init.clear()
        dfa.finals.clear()
        dfa.states.clear()
        dfa.transitions.clear()
        dfa.add_state(join(self.init,",")) #create a unique init state and add to states variable
        dfa.init=[] #create an empty list of init state conform to our class structure 
        dfa.init.append(join(self.init,",")) # add the unique init state inside
        r=0 # there is the number of state pass by determinism method
        while r<len(dfa.states):
            for symbol in dfa.alphabet :
                temp_states=dfa.states[r].split(",") # split our current in his substates
                j=0
                new_state=""
                while j<len(temp_states):
                    if len(self.dst_state(temp_states[j],symbol))>0: #check all destination state base on current symbol
                        k=0
                        while k<len(self.dst_state(temp_states[j],symbol)): #fetch all destination states
                            if new_state=="":
                                new_state=self.dst_state(temp_states[j],symbol)[k] #add those states in the new state
                            else : 
                                if self.dst_state(temp_states[j],symbol)[k] not in new_state.split(","): # check if the destination state is not yet exist in new state variable
                                    
                                    new_state=new_state+","+self.dst_state(temp_states[j],symbol)[k] #add those states in the new state
                            k=k+1
                    j=j+1
                if new_state!="":
                    temp=new_state.split(",") #split the new state as list
                    temp.sort() #sort the list
                    new_state=join(temp,",") #reconvert the list of states to string
                    final=False #flag to know if content a final state
                    if new_state not in dfa.states: # check if new state is newer
                        for f in self.finals : # loop for checking if new state content final state
                            if f in new_state.split(","): # check if inide
                                final=True
                                break #break if final state showed
                        dfa.add_state(new_state,init=False,final=final) #add our new state
                        #print(new_state)
                        #print(final)
                    dfa.add_transition(dfa.states[r],symbol,new_state) #add transition to our new state
                    # print("transition de %s vers %s sur %s "%(dfa.states[r],new_state,symbol))
            r=r+1
        dfa.name = "determinism({})".format(self.name)
        return dfa 

