# -*- coding: utf8 -*-
# dfa.py 1.0
# Copyright Djiembou Victor (15/06/2021)
##
# git@gitlab.com:Viclegrand28/info304.git
# https://gitlab.com/Viclegrand28/info304.git
##
# Viclegranddab@gmail.com


from package.fa import FA
from package.functions import *
import copy

__version__ = '1.0'


def Property(func):
    return property(**func())


class DFA(FA):
    """ This class represent any type of deterministic finite automaton."""

    def __init__(self, alphabet="", states=[], transitions={}, finals=[], init=[]):
        """ Initialise the finite automaton.
            @param the alphabet of the automaton."""
        FA.__init__(self,alphabet, states, transitions, finals, init)

    def negate(self):
        """[summary]
        negate of FA
        """
        if not self.is_complete():
            print("error : negation requires a complete DFA.")
            return
        
        negate = DFA(self.alphabet.copy(),self.states.copy(), self.transitions.copy(), self.finals.copy(), self.init.copy())
        oldfinals = dfa.finals.copy()
        negate.finals.clear()
        #print(""".DS_Store {}, {}, {}""".format(negate.finals, negate.states,oldfinals))
        for state in negate.states:
            if state not in oldfinals:
                negate.finals.append(state)
        negate.name = "negate({})".format(self.name)
        return negate