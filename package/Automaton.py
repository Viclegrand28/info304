import networkx as nx
import matplotlib.pyplot as plt
import os
import sys
import subprocess
from IPython.display import Image
import copy
#import pygraphviz
class Automaton:
    """ This class represent any type of deterministic finite automaton."""
    def __init__(self, alphabet):
        """ Initialise the finite autaomaton.
            @param the alphabet of the automaton."""
        self.name = ""
        """ List of string corresponding to states name.
            States are always identificated by name."""
        self.states = []
        """ Dictionary using src state as key and mapping it to a list of
            pair (dest_state, symbol)."""
        self.transitions = {}
        """ The string corresponding to the name of the initial state."""
        self.init = []
        """ A list containing the name of the final states."""
        self.finals = []
        """ A string containing all symbols in the alphabet."""
        self.alphabet = ""
        for s in alphabet:
            if s not in self.alphabet:
                self.alphabet += s   

    def setName(self, name):
        self.name = name
    
    def getName(self):
        return self.name
        
    def add_state(self, state, final = False):
        if state in self.states:
            print("error : state '" + state + "' already exists.")
            return
        self.transitions[state] = []
        self.states.append(state)
        if final:
            self.finals.append(state)
    def valid_symbol(self, symbol):
        if symbol not in self.alphabet: return False
        return True
    def dst_state(self, src_state, symbol):
        if src_state not in self.states:
            print("error : the state '" + src_state + "' is not an existing state.")
            return
        dst_states=[]
        for (s, dst_state) in self.transitions[src_state]:
            if s == symbol:
                dst_states.append(dst_state)
        return dst_states
 
    def add_transition(self, src_state, symbol, dst_state):
        if not self.valid_symbol(symbol):
            print("error : the symbol '" + symbol + "' is not part of the alphabet.")
            return
        if src_state not in self.states:
            print("error : the state '" + src_state + "' is not an existing state.")
            return
        if dst_state not in self.states:
            print("error : the state '" + dst_state + "' is not an existing state.")
            return
        if dst_state in self.dst_state(src_state, symbol):
            print("error : the transition (" + src_state + ", " + symbol + "," + dst_state +") already exists.")
            return
        self.transitions[src_state].append((symbol, dst_state))
    def __str__(self):
        ret = "FA :\n"
        ret += "-alphabet:'" + self.alphabet + "'\n"
        ret += "-init:" + str(self.init) + "\n"
        ret += "-finals:" + str(self.finals) + "\n"
        ret += "- states (%d) :\n" % (len(self.states))
        for state in self.states:
            ret += "- (%s)" % (state)
            if len(self.transitions[state]) == 0:
                ret += ".\n"
            else:
                ret += ret + ":\n"
                for (sym, dest) in self.transitions[state]:
                    ret += ret + "--(%s)--> (%s)\n" % (sym, dest)
        return ret
    def run(self, word, verbose = False):
        if len(self.init) == 0:
            print("error : the automaton does not have any initial symbol.")
            return False
        current_state = self.init
        pos=1
        i = 0
        for symbol in word:
            k=0
            next_state=[]
            while k<len(current_state): 
                next_state = next_state + self.dst_state(current_state[k], symbol) +self.dst_state(current_state[k],"E")
                l=self.dst_state(current_state[k], symbol)
                if verbose : 
                    print("configuration : (" + current_state[k] + ", " + word[i:] + ") : -> ") 
                    print(self.dst_state(current_state[k], symbol))
                if not self.valid_symbol(symbol):
                    print("error : the symbol '" + symbol + "' is not part of the alphabet. Abord.")
                    return False
                k=k+1
                pos=pos+1
                if(len(next_state))==0 : 
                    print("no transition available for ( " + symbol +")")
                    return False
                current_state=next_state
            i = i+1
        k=0
        result=False
        while k<len(current_state):
            if current_state[k] in self.finals:
                result=result or True
            else :
                result=result or False
            k=k+1
        return result
    def to_dot(dfa, name="Graphe"):
        def join(a_list,sym):
            result=""
            k=0
            while k<len(a_list):
                if result=="":
                    result=a_list[k]
                else :
                    result=result+sym+a_list[k]
                k=k+1
            return result
        ret = "digraph " + name + " {\n    rankdir=\"LR\";\n\n"
        ret += "    // States (" + str(len(dfa.states)) + ")\n"
 
        state_name = lambda s : "Q_" + str(dfa.states.index(s))
 
        # States
        for i in dfa.init :
            ret += "    node [shape = point ];     __Qi__ // Initial state\n" # Initial state
        for state in dfa.states:
            ret += "    "
            if state in dfa.finals:
                ret += "node [shape=doublecircle]; "
            else:
                ret += "node [shape=circle];       "
            ret += state_name(state) + " [label=" + join(state.split(","),"") + "];\n"
 
         # Transitions
        ret += "\n    // Transitions\n"
        for i in dfa.init:
            ret += "    __Qi__ -> " + state_name(i) + "; // Initial state arrow\n"  
        for state in dfa.states:
            for (symbol, dst_state) in dfa.transitions[state]:
                if symbol=="":
                    ret += "    " + state_name(state) + " -> " + state_name(dst_state) + " [label=" + "E" + "];\n"
                else :
                    ret += "    " + state_name(state) + " -> " + state_name(dst_state) + " [label=" + symbol + "];\n"
        ret+="}\n"
        tmp_file ="graphe.dot"
        with open(tmp_file, "w") as file:
            file.write(ret)
        return tmp_file
    def display(dfa):
        dfa.to_dot()
        #subprocess.call("dot -Tpng graph.dot -o test2.png")
        #return Image(filename =r'test2.png')