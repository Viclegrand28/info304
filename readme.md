# TP INFO 304
# 17T2051 DJIEMBOU TIENTCHEU VICTOR NICO

# TOPIC : IMPLEMENT ALL AUTOMATA ALGORITHMS SAW AT THE COURSE

* Steps ;
	* [x] implement finite automaton class 
		* [x] init method 
			* [x] with alphabel, states, transition, init_state and final_state arguments
		* [x] add state method
		* [x] valid symbol method
		* [x] add transition method
		* [x] str method
		* [x] word recognize method
		* [x] display ipython method
		* [x] save automate method 
		* [x] mirror method
		* [x] type automaton method
		* [x] accessible verification state method
		* [x] coacessible verification state method
		* [x] usefull verification state method
		* [x] emonde verification automate method
		* [x] product of two automata method
		* [x] intersect of two automata method
		* [x] difference of two automata method
		* [x] union of two automata method
	* [ ] implement deterministic finite automate class
		* [ ] minimize method
		* [x] negate method
		* [ ] canonique method
	* [x] implement non deterministic finite automate class
		* [x] transform automate to DFA method
	* [x] implement epsilon non deterministic fine automate class
		* [x] transform automate to NFA method
	* [ ] implement regular or rational expression
    	* [ ] transform RE to NFA
    	* [ ] transform RE to E-NFA
		