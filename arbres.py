# -*- coding: utf8 -*-
## abres.py 1.0
## Copyright Djiembou Victor (31/12/2020)
##
## Viclegranddab@gmail.com
##
## Ce logiciel est un module Python
## permettant de manipuler des arbres


import subprocess

__version__ = '1.0'

def Property(func) :
    return property(**func())

#def _isa(instance, classe ) :
#    if instance.__class__==classe : return True
#    for b in instance.__bases__ :
#        if _isa(b,classe) : return True
#    return False
class Arbre :
    """ Classe pour représenter des arbres """

    def __fromlist(self,liste) :
        """ Construit un arbre à partir de listes """
        assert(isinstance(liste, list) or isinstance(liste, tuple))
        assert(len(liste)>=1)
        self.__label=liste[0]
        for sa in liste[1:] :
            self._fils.append(Arbre(sa))

    def __init__(self, first, **kwargs) :#label, listefils=None) :
        """
        Crée un arbre à partir
        1) d'un label et d'une liste de sous arbres éventuels
        2) Si le premier argument est une liste ou un tuple, il doit
           "bien construit" : en premier élément un label et en second
           élément une liste de fils suivant le même schéma.
        3) Un autre arbre (copie en profondeur)
        Exemple :
        a=Arbre('A',fils=[Arbre('B',Arbre('C')])
        b=Arbre(( 'A',(('B',('C')),'D') ))

        L'étiquette d'un noeud ne DOIT PAS être None
        """
        assert(first!=None)
        self.__label=None
        self._fils=[]
        if isinstance(first,list) or isinstance(first,tuple) :
            self.__fromlist(first)
        elif isinstance(first,Arbre) :
            self.__label=first.racine
            for sa in first._fils :
                if sa==None : self._fils.append(None)
                else : self._fils.append(Arbre(sa))
        else :
            self.__label=first
            self._fils=kwargs.get('fils',[])
    

    def getLabel(self):
    	return self.__label


    def __get_racine(self) :
        return self.__label
    def __set_racine(self, label) :
        self.__label=label
    racine=property(__get_racine,__set_racine,doc="Accède ou modifie le label de la racine")

    def ajoute(self,a) :
        """ Ajoute l'arbre a comme nouveau fils de la racine
        (en fin de liste)
        """
        assert(isinstance(a,Arbre))
        self._fils.append(a)

    def remplace(self,pos,a) :
        """ Remplace le fils de la racine situé en position pos par
        l'arbre a.
        """
        if pos<0 : pos=len(self._fils)-pos
        if pos<0 : raise IndexError('Position '+pos+' incorrecte')
        if pos>=len(self._fils) : raise IndexError('Position '+pos+' incorrecte')
        self._fils[pos]=a

    def __iter__(self) :
        """ Itère sur les fils. Si un fils vaut 'None' il est ignoré """
        for l in self._fils :
            if l!= None :
                yield l
    def lesfils(self) :
        return self._fils
    def __getitem__(self,i) :
        """ Renvoie une référence vers le fils numéro i
        Exemple :
        a=Arbre((4,5,(6,7,8)))
        b=a[1]
        print(b)
        >>> (6-(7,8))

        Lève IndexError si l'indice es incorrect
        """
        if i<0 : i<len(self._fils)-i
        if i not in range(len(self._fils)) :
            raise IndexError('Pas de fils '+str(i))
        return self._fils[i]

    def __setitem__(self,i,v) :
        """ Modifie le fils numéro i s'il existe et l'ajoute sinon
        """
        if i<0 : i<len(self._fils)-i
        if i<0 : raise IndexError('Pas de fils '+str(i))
        while len(self._fils)<=i : self._fils.append(None)
        if v.__class__==self.__class__ :
            self._fils[i]=v
        else :
            # Ci-dessous : à retenir, appelle Arbre(v) ou ArbreBinaire(v)
            # selon le type de self
            self._fils[i]=self.__class__(v)

    def __str__(self) :
        """ Renvoie une représentation lisible de l'objet sous
        forme de chaîne de caractères
        """
        s=[]
        for l in self._fils :
            if l==None : s.append('')
            else :s.append(str(l))
        # return str(s)
        s=",".join(s)
        # return str(s)
        if s!="" : return "("+str(self.__label)+"-("+s+"))"
        else : return str(self.__label)

    def __len__(self) :
        """ Renvoie le nombre de fils (différents de None) """
        return len([f for f in self._fils if f !=None])

    def __repr__(self) :
        if len(self._fils)==0 : return self.__class__.__name__+"("+repr(self.racine)+")"
        f=[]
        for l in self._fils :
            if l==None : f.append(repr(None))
            else :
                f.append(repr(l))
            pass
        return self.__class__.__name__+"(("+repr(self.racine)+","+",".join(f)+"))"

    def _innerdot(self) :
        stri=""
        for a in self :
            stri+="{0} -> {1} [label=\"\"];\n".format(id(self),id(a))
        col="white"
        infos=str(self.racine)
        # print("{} {} {}".format(infos,self, len(self)))
        attr="fillcolor=\"#888888ff\""
        stri+='\"{label}\"  [label=\"{infos}\", style = filled, peripheries = 1, \
                fillcolor = {col}, color = black];\n'.format(label=id(self),infos=infos,col=col)
        for a in self :
            stri+=a._innerdot()
        return stri

    def _loudsencoding(self) :
        stri=""
        for a in self :
            stri+="{0} -> {1} [label=\"\"];\n".format(id(self),id(a))
        col="white"
        infos=str(self.racine)
        # print("{} {} {}".format(infos,self, len(self)))
        attr="fillcolor=\"#888888ff\""
        stri+='\"{label}\"  [label=\"{infos}\", style = filled, peripheries = 1, \
                fillcolor = {col}, color = black];\n'.format(label=id(self),infos="1"*len(self)+"0",col=col)
        for a in self :
            stri+=a._loudsencoding()
        return stri

    def dot(self,printinfos=True) :
        """ Crée une chaîne de caractère contenant une
        description de l'arbre pour le programme dot (graphviz)
        """
        return "digraph g {\n"+self._innerdot()+"}\n"

    def dotlouds(self,printinfos=True) :
        """ Crée une chaîne de caractère contenant une
        description de l'arbre pour le programme dot (graphviz)
        """
        liste = Arbre(10)
        liste.ajoute(self)
        # print("*"*100)
        # print(liste)
        # print("*"*100)
        # tets = Arbre(10,fils=[self.racine,self])
        # print(tets)	
        return "digraph g {\n"+liste._loudsencoding()+"}\n"

    def save(self,filename='graphout.png') :
        """ Sauvegarde une image de l'abre dans un fichier (Graphviz nécessaire)"""
        try :
            pipe=subprocess.Popen(['dot','-Tpng','-o',filename],stdin=subprocess.PIPE)
            pipe.stdin.write(self.dot().encode('ascii') + b'\n')
            pipe.stdin.close()
            pipe.wait()
        except OSError as e :
            print("Impossible de Tracer le graphe...")

    def savelouds(self,filename='loudsgraphout.png') :
        """ Sauvegarde une image de l'abre dans un fichier (Graphviz nécessaire)"""
        try :
            pipe=subprocess.Popen(['dot','-Tpng','-o',filename],stdin=subprocess.PIPE)
            pipe.stdin.write(self.dotlouds().encode('ascii') + b'\n')
            pipe.stdin.close()
            pipe.wait()
        except OSError as e :
            print("Impossible de Tracer le graphe...")


class File:
	
	def __init__(self) :
		self.content = []

	def __str__(self) :
		s=",".join(self.content)
		if s!="" : return "["+s+"]"
		else : return "empty"

	def enqueue(self, element):
		self.content.append(element)

	def dequeue(self):
		return self.content.pop(0)

	def isempty(self):
		return len(self.content) <= 0

	def length(self):
		return len(self.content)

class Algth2_LOUDSencoding :

	def __init__(self):
		self.T = Arbre((1,(2,5,6),3,(4,7,(8,10),9)))
		self.f = File()
		# self.BFS = File()
		self.CODE = ["10"]
		self.CODES = [1,0] # useful
		self.k = 0 # useful
		self.i = 0

	# def BFS(self):
	# 	for i in range(len(self.content)):
	# 		for j in range(self.content[i].count("1")):
				

	def main(self):
		self.f.enqueue(self.T)
		print("#"*100)
		# self.BFS.enqueue(self.T)
		test = []
		while self.f.isempty() != True:
			self.k = 0
			t = self.f.dequeue()
			# print(t)
			test.append(t.getLabel())
			for i in range(len(t)):
				if t[i] != None:
					self.f.enqueue(t[i])
					# self.BFS.enqueue(t[i])
					self.k+=1
					# print(t[i])
					# test.append(t[i].getLabel())
			for i in range(self.k):
				self.CODES.append(1)
			self.CODES.append(0)
			self.CODE.append("1"*self.k+"0")
		# print(self.CODE)
		print(self.CODES)
		# print(test)

"""General method of succint data representation
"""
def Rank(b, S, k):

	if isinstance(S, list):
		if str(k).isdigit():
			return S[:k].count(b)
		else:
			print("the S parameter need to be an Integer but {} was gived ".format(type(S)))
	else:
		print("the S parameter need to be a List but {} was gived ".format(type(S)))

def Select(b, S, k):

	if isinstance(S, list):
		if str(k).isdigit():
			out = 0
			slt = None
			i = 0
			cpt = len(S)
			while out != 1:
				if Rank(b, S, i) == k:
					slt = i
					out = 1

				if i<cpt:
					i+=1
				else:
					out = 1
			return slt if slt != None else "b is not in the list"
		else:
			print("the S parameter need to be an Integer but {} was gived ".format(type(S)))
	else:
		print("the S parameter need to be a List but {} was gived ".format(type(S)))


def Succ(p, S, b):

	if isinstance(S, list):
		if str(b).isdigit():
			return Select(b, S, Rank(b, S, p) + 1) if slt != None else "b has not next occurence in the list"
		else:
			print("the S parameter need to be an Integer but {} was gived ".format(type(S)))
	else:
		print("the S parameter need to be a List but {} was gived ".format(type(S)))


def Pred(p, S, b):

	if isinstance(S, list):
		if str(b).isdigit():
			return Select(b, S, Rank(b, S, p) - 1) if slt != None else "b has not next occurence in the list"
		else:
			print("the S parameter need to be an Integer but {} was gived ".format(type(S)))
	else:
		print("the S parameter need to be a List but {} was gived ".format(type(S)))





if __name__=='__main__' :
    ar=Arbre((1,(2,5,(6,11)),3,(4,7,(8,10),9)))
    # d=ArbreBinaire(4,fils=(ArbreBinaire(3),Arbre(7)))
    print(ar)
    # print(d)
    ar.save()
    ar.savelouds()
    # print(ar.__getattribute__(label)) 
    Algth2_LOUDSencoding().main()