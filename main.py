from package.fa import NFA, DFA, ENFA
from package.thompson import regex_to_ndfa

if __name__ == '__main__':
    a = NFA("ab")
    abcd = DFA("ab",a.states.copy(), a.transitions.copy(), a.finals.copy(), a.init.copy())
    abcd.add_state("0", init=True, final=True)
    abcd.add_state("1", init=False, final=False)
    abcd.add_state("2", init=False, final=False)
    abcd.add_transition("0", "a", "1")
    abcd.add_transition("0", "b", "0")
    abcd.add_transition("1", "b", "1")
    abcd.add_transition("2", "b", "2")
    abcd.add_transition("2", "a", "0")
    abcd.add_transition("1", "a", "2")
    abcd.setName("complete")
    abcde = abcd.complete()
    abcde.display()
    print(abcde)